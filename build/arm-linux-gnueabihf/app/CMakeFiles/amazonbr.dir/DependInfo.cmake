# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/insanicth/amazonbr/build/arm-linux-gnueabihf/app/CMakeFiles/amazonbr.dir/qrc_qml.cpp" "/home/insanicth/amazonbr/build/arm-linux-gnueabihf/app/CMakeFiles/amazonbr.dir/CMakeFiles/amazonbr.dir/qrc_qml.cpp.o"
  "/home/insanicth/amazonbr/build/arm-linux-gnueabihf/app/amazonbr_automoc.cpp" "/home/insanicth/amazonbr/build/arm-linux-gnueabihf/app/CMakeFiles/amazonbr.dir/amazonbr_automoc.cpp.o"
  "/home/insanicth/amazonbr/main.cpp" "/home/insanicth/amazonbr/build/arm-linux-gnueabihf/app/CMakeFiles/amazonbr.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_QML_LIB"
  "QT_QUICKCONTROLS2_LIB"
  "QT_QUICK_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/arm-linux-gnueabihf/qt5"
  "/usr/include/arm-linux-gnueabihf/qt5/QtGui"
  "/usr/include/arm-linux-gnueabihf/qt5/QtCore"
  "/usr/lib/arm-linux-gnueabihf/qt5/mkspecs/linux-g++"
  "/usr/include/arm-linux-gnueabihf/qt5/QtQml"
  "/usr/include/arm-linux-gnueabihf/qt5/QtNetwork"
  "/usr/include/arm-linux-gnueabihf/qt5/QtQuick"
  "/usr/include/arm-linux-gnueabihf/qt5/QtQuickControls2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
